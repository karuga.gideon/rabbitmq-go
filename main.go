package main

import (
	"fmt"
	"rabbitmq-go/rabbitmq"
	"strconv"
	"time"
)

func processMessages(i int) {
	rabbitmq.SendMessage("Custom Message >>> " + strconv.Itoa(i))
	time.Sleep(2 * time.Second)
}

func main() {

	fmt.Println("Rabbit MQ Message Processing...")

	for i := 1; i < 11; i++ {
		processMessages(i)
	}

}
